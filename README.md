# Creative Dock bootcamp

## Base dependencies
  - Git
  - Node.js v8.11+
  - Npm 5.6.0+

## Install instructions
    1. git clone https://czekFREE@bitbucket.org/czekFREE/creative-dock-bootcamp.git
    2. cd creative-dock-bootcamp
    3. npm install
    4. npm start

## Test instructions
    1. npm test

## Build info & project structure
    - This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
