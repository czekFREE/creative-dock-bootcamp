import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ArrowRight extends Component {
  static propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
  };

  static defaultProps = {
    width: 24,
    height: 24,
    color: '',
    className: '',
  };

  render() {
    const { width, height, color, className, restProps } = this.props;
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={width}
        height={height}
        viewBox="0 0 24 24"
        className={className}
        {...restProps}
      >
        <path
          fill={color}
          d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"
        />
        <path fill="none" d="M0 0h24v24H0V0z" />
      </svg>
    );
  }
}

export default ArrowRight;
