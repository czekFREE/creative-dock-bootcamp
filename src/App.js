import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import SurveyForm from './modules/SurveyForm/SurveyForm';

import SurveyPreview from './modules/SurveyPreview/SurveyPreview';

import './App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <SurveyForm />
          <SurveyPreview />
        </div>
      </Provider>
    );
  }
}

export default App;
