import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

// import surveyPreviewReducer from './modules/SurveyPreview/reducer';
// import surveyFormReducer from './modules/SurveyForm/reducer';
//
// import { MODULE_NAME as SURVEY_PREVIEW_NAMESPACE } from './modules/SurveyPreview/constants';
// import { MODULE_NAME as SURVEY_FORM_NAMESPACE } from './modules/SurveyForm/constants';

const rootReducer = combineReducers({
  // [SURVEY_PREVIEW_NAMESPACE]: surveyPreviewReducer,
  // [SURVEY_FORM_NAMESPACE]: surveyFormReducer,
  form: formReducer,
});

export default rootReducer;
