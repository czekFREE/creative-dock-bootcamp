import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { PATTERN_1, PATTERN_2, PATTERN_3, PATTERN_4 } from './constants';
import './SurveyForm.scss';

class SurveyForm extends Component {
  static propTypes = {};

  generatePatternComponent = (pattern) => (props) => (
    <button
      className="SurveyForm__patterns__button"
      type="button"
      onClick={() => props.input.onChange(pattern)}
    >
      {pattern}
    </button>
  );

  patternComponent1 = this.generatePatternComponent(PATTERN_1);
  patternComponent2 = this.generatePatternComponent(PATTERN_2);
  patternComponent3 = this.generatePatternComponent(PATTERN_3);
  patternComponent4 = this.generatePatternComponent(PATTERN_4);

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="SurveyFormWrapper">
        <form action="#" className="SurveyForm" onSubmit={handleSubmit}>
          <div className="SurveyForm__controlsGroup">
            <label className="SurveyForm__label" htmlFor="pattern">
              1. Pattern of the survey
            </label>
            <div className="SurveyForm__patterns">
              <Field name="pattern" component={this.patternComponent1} />
              <Field name="pattern" component={this.patternComponent2} />
              <Field name="pattern" component={this.patternComponent3} />
              <Field name="pattern" component={this.patternComponent4} />
            </div>
          </div>

          <div className="SurveyForm__controlsGroup">
            <label className="SurveyForm__label" htmlFor="title">
              2. Title of the survey
            </label>
            <Field
              className="SurveyForm__title"
              name="title"
              component="input"
              type="text"
            />
          </div>
          <div className="SurveyForm__controlsGroup">
            <label className="SurveyForm__label" htmlFor="description">
              3. Description of the survey
            </label>
            <Field
              className="SurveyForm__description"
              name="description"
              component="textarea"
              rows={8}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  null,
)(
  reduxForm({
    form: 'SurveyForm',
  })(SurveyForm),
);
