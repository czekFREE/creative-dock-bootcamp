export const PATTERN_1 = 'pattern-1';
export const PATTERN_2 = 'pattern-2';
export const PATTERN_3 = 'pattern-3';
export const PATTERN_4 = 'pattern-4';
export const DEFAULT_PATTERN = PATTERN_1;

export const MODULE_NAME = 'SurveyForm';
