import React from 'react';
import { shallow, mount } from 'enzyme';
import {
  SurveyPreview,
  DEFAULT_TITLE,
  DEFAULT_DESCRIPTION,
} from '../SurveyPreview';
import {
  PATTERN_1,
  PATTERN_2,
  PATTERN_3,
  PATTERN_4,
  DEFAULT_PATTERN,
} from '../../SurveyForm/constants';

describe('<SurveyPreview />', () => {
  it('Renders without crashing', () => {
    const wrapper = shallow(<SurveyPreview />);

    expect(wrapper.find('.SurveyPreview').length).toEqual(1);
  });

  it('Renders default title when title provided via props is undefined', () => {
    const wrapper = mount(<SurveyPreview />);

    expect(wrapper.find('.SurveyPreview__title').text()).toEqual(DEFAULT_TITLE);
  });

  it('Renders passed in title', () => {
    const title = 'Sample title';
    const wrapper = mount(<SurveyPreview title={title} />);

    expect(wrapper.find('.SurveyPreview__title').text()).toEqual(title);
  });

  it('Renders default description when title provided via props is undefined', () => {
    const wrapper = mount(<SurveyPreview />);

    expect(wrapper.find('.SurveyPreview__description').text()).toEqual(
      DEFAULT_DESCRIPTION,
    );
  });

  it('Renders passed in title', () => {
    const description = 'Sample description';
    const wrapper = mount(<SurveyPreview description={description} />);

    expect(wrapper.find('.SurveyPreview__description').text()).toEqual(
      description,
    );
  });

  it('Renders default pattern className when pattern provided via props is undefined', () => {
    const wrapper = mount(<SurveyPreview />);

    expect(
      wrapper.find('.SurveyPreview__pattern').hasClass('pattern-1'),
    ).toEqual(true);
  });

  it('Renders pattern-1 className when pattern provided via props is PATTERN_1', () => {
    const wrapper = mount(<SurveyPreview pattern={PATTERN_1} />);

    expect(
      wrapper.find('.SurveyPreview__pattern').hasClass('pattern-1'),
    ).toEqual(true);
  });

  it('Renders pattern-2 className when pattern provided via props is PATTERN_2', () => {
    const wrapper = mount(<SurveyPreview pattern={PATTERN_2} />);

    expect(
      wrapper.find('.SurveyPreview__pattern').hasClass('pattern-2'),
    ).toEqual(true);
  });

  it('Renders pattern-3 className when pattern provided via props is PATTERN_3', () => {
    const wrapper = mount(<SurveyPreview pattern={PATTERN_3} />);

    expect(
      wrapper.find('.SurveyPreview__pattern').hasClass('pattern-3'),
    ).toEqual(true);
  });

  it('Renders pattern-4 className when pattern provided via props is PATTERN_4', () => {
    const wrapper = mount(<SurveyPreview pattern={PATTERN_4} />);

    expect(
      wrapper.find('.SurveyPreview__pattern').hasClass('pattern-4'),
    ).toEqual(true);
  });
});
