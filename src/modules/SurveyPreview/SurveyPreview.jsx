import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import { MODULE_NAME as SURVEY_FORM_NAMESPACE } from '../SurveyForm/constants';
import {
  PATTERN_1,
  PATTERN_2,
  PATTERN_3,
  PATTERN_4,
  DEFAULT_PATTERN,
} from '../SurveyForm/constants';
import ArrowRight from '../../components/Icons/ArrowRight/ArrowRight';
import Close from '../../components/Icons/Close/Close';

import './SurveyPreview.scss';

export const DEFAULT_TITLE = 'Title placeholder';
export const DEFAULT_DESCRIPTION = 'Description placeholder';

export class SurveyPreview extends Component {
  static propTypes = {
    pattern: PropTypes.oneOf([PATTERN_1, PATTERN_2, PATTERN_3, PATTERN_4]),
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  };

  static defaultProps = {
    pattern: DEFAULT_PATTERN,
    title: DEFAULT_TITLE,
    description: DEFAULT_DESCRIPTION,
  };

  render() {
    const { pattern, title, description } = this.props;

    return (
      <div className="SurveyPreview">
        <div className="SurveyPreview__descriptionTips">
          <div className="SurveyPreview__descriptionTips__item item1">1</div>
          <div className="SurveyPreview__descriptionTips__item item2">2</div>
          <div className="SurveyPreview__descriptionTips__item item3">3</div>
        </div>

        <div className="SurveyPreview__innerWrapper">
          <div className={`SurveyPreview__pattern ${pattern}`}>
            <div className="SurveyPreview__close">
              <Close width={12} height={12} color="#71859B" />
            </div>
          </div>
          <div className="SurveyPreview__mobileBg">
            <div className="SurveyPreview__mobileBg__innerWrapper">
              <div className="SurveyPreview__content">
                <div className="SurveyPreview__contentHeader">
                  <span className="SurveyPreview__contentHeader__timer noselect">
                    20 days left
                  </span>
                  <span className="SurveyPreview__contentHeader__reward noselect">
                    Reward{' '}
                    <span className="SurveyPreview__contentHeader__reward__points noselect">
                      100{' '}
                      <span className="SurveyPreview__contentHeader__reward__points__currency noselect">
                        VLD
                      </span>
                    </span>
                  </span>
                </div>

                <h1 className="SurveyPreview__title noselect">{title}</h1>
                <h2 className="SurveyPreview__subTitle noselect">
                  By Trust Square
                </h2>
                <div className="SurveyPreview__description noselect">
                  {description}
                </div>
              </div>
              <div className="SurveyPreview__requests">
                <div className="SurveyPreview__requests__header noselect">
                  The contractor requests following data:
                </div>

                <div className="SurveyPreview__requests__item">
                  <ArrowRight
                    className="SurveyPreview__requests__item__icon"
                    width={14}
                    height={14}
                    color="#d0d0d0"
                  />
                  <span className="SurveyPreview__requests__item__text noselect">
                    Personal data:
                  </span>
                  <span className="SurveyPreview__requests__item__notice">
                    <span className="SurveyPreview__requests__item__notice__text noselect">
                      Missing
                    </span>
                  </span>
                </div>
              </div>

              <div className="SurveyPreview__submit">
                <div className="SurveyPreview__submit__button noselect">
                  Take Survey
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const formValues = getFormValues(SURVEY_FORM_NAMESPACE)(state);
  const pattern = formValues ? formValues.pattern : undefined;
  const title = formValues ? formValues.title : undefined;
  const description = formValues ? formValues.description : undefined;

  return {
    pattern,
    title,
    description,
  };
};

export default connect(
  mapStateToProps,
  null,
)(SurveyPreview);
